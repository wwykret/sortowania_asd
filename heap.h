#ifndef SORTOWANIA_ASD_HEAP_H
#define SORTOWANIA_ASD_HEAP_H


class Heap {
public:
    int* arr;
    int len;

    Heap(int* arr_, int n_);
    ~Heap();
    void Heapify(int n, int i);
    void BuildHeap(int n);
    void HeapSort();
    int ExtractMax();
    void Insert(int val);
    void Swap(int a, int b);
};


#endif //SORTOWANIA_ASD_HEAP_H
