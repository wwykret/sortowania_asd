#ifndef SORTOWANIA_ASD_SORTER_H
#define SORTOWANIA_ASD_SORTER_H

#include <iostream>

class Sorter {
private:
    int* arr;
    int n;

public:
    Sorter();
    Sorter(int* arr_, int n_);
    ~Sorter();
    void SetArray(int* arr_, int n_);
    void InsertionSort();
    void MergeSort();
    void MergeSort(int arr[], int l, int r);
    void Merge(int arr[], int l, int m, int r);
    void HeapSort();
    bool IsArraySorted();
    void DisplayArray();

};


#endif //SORTOWANIA_ASD_SORTER_H
