#include "heap.h"
#include <stdlib.h>

Heap::Heap(int *arr_, int n) {
    arr = new int[n];
    for (int i = 0; i < n; i++) {
        arr[i] = arr_[i];
    }
    len = n;
}

Heap::~Heap() {
    delete(arr);
}

void Heap::Heapify(int n, int i) {
    int largest = i;
    int l = (i << 1) + 1;
    int r = (i << 1) + 2;

    //czy lewe dziecko jest mniejsze od rodzica
    if (l < n && arr[l] > arr[largest])
        largest = l;

    //czy prawe dziecko jest mniejsze od rodzica
    if (r < n && arr[r] > arr[largest])
        largest = r;

    // czy rodzic nie jest najwiekszy
    if (largest != i)
    {
        Swap(i, largest);

        // Recursively heapify the affected sub-tree
        Heapify(n, largest);
    }
}

void Heap::BuildHeap(int n) {
    for (int i = (n >> 1) - 1; i >= 0; i--)
        Heapify(n, i);
}

void Heap::HeapSort() {
    // budowanie kopca
    BuildHeap(len);

    // usuwanie najwiekszego elementu
    for (int i = len - 1; i > 0; i--) {
        Swap(0, i);

        // ponowne odtwarzanie kopca
        Heapify(i, 0);
    }
}

void Heap::Swap(int a, int b) {
    int temp = arr[a];
    arr[a] = arr[b];
    arr[b] = temp;
}

int Heap::ExtractMax() {
    if (len <= 0) return 0;

    int val = arr[0];
    Swap(0, len - 1);
    len--;
    Heapify(len, 0);
    arr = (int*) realloc(arr, sizeof(int) * len);
    return val;
}

void Heap::Insert(int val) {
    len++;
    arr = (int*) realloc(arr, sizeof(int) * len);
    arr[len - 1] = val;

    int i = len - 1;
    while(i > 0) {
        if (arr[i >> 1] < arr[i]) {
            Swap(i >> 1, i);
        }
        i >>= 1;
    }
}