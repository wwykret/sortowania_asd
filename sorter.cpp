#include "sorter.h"
#include "heap.h"

Sorter::Sorter() {
    n = -1;
    arr = nullptr;
}

Sorter::Sorter(int *arr_, int n_) {
    SetArray(arr_, n_);
}

Sorter::~Sorter() {
    delete[] arr;
}

void Sorter::SetArray(int *arr_, int n_) {
    n = n_;
    arr = arr_;
}

bool Sorter::IsArraySorted() {
    if (n < 0) return false;
    else if (n == 0) return true;

    int biggest = arr[0];
    for (int i = 0; i < n; i++) {
        if (biggest > arr[i]) return false;
        else biggest = arr[i];
    }

    return true;
}

void Sorter::InsertionSort() {
    for (int j = 1; j <= n; j++) {
        int element = arr[j];
        int i = j - 1;
        while (i >= 0 && arr[i] > element) {
            arr[i + 1] = arr[i];
            i -= 1;
        }
        arr[i + 1] = element;
    }
}

void Sorter::DisplayArray() {
    for (int i = 0; i < n; i++) {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;
}

void Sorter::MergeSort() {
    MergeSort(arr, 0, n - 1);
}

void Sorter::MergeSort(int array[], int l, int r) {
    if (r <= l) return;

    int m = l + (r - l) / 2;

    MergeSort(array, l, m);
    MergeSort(array, m + 1, r);

    Merge(array, l, m, r);
}

void Sorter::Merge(int array[], int l, int m, int r) {
    int n1 = m - l + 1;
    int n2 = r - m;

    int L[n1], R[n2];

    for (int i = 0; i < n1; i++)
        L[i] = array[l + i];
    for (int j = 0; j < n2; j++)
        R[j] = array[m + 1 + j];

    int i = 0;
    int j = 0;
    int k = l;

    while (i < n1 && j < n2) {
        if (L[i] <= R[j]) {
            array[k] = L[i];
            i++;
        } else {
            array[k] = R[j];
            j++;
        }
        k++;
    }
    while (i < n1) {
        array[k] = L[i];
        i++;
        k++;
    }
    while (j < n2) {
        array[k] = R[j];
        j++;
        k++;
    }
}

void Sorter::HeapSort() {
    Heap heap(arr, n);

    heap.HeapSort();
    n--;

    for (int i = 0; i < n; i++) {
        arr[i] = heap.arr[i];
    }
}